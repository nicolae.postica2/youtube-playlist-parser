const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const fs = require('fs');

playlistUrl = process.argv[2];

const parser = (res) => {
  const $ = cheerio.load(res);
  const thumb = $('a.yt-simple-endpoint.style-scope.ytd-playlist-video-renderer');
  let arr = [];
  thumb.map((index, el) => {
    arr.push(el.attribs.href.split('&list=')[0].split('/watch?v=')[1])
  });
  return arr;
};

const autoScroll = async (page) => {
  await page.evaluate(async () => {
    await new Promise((resolve) => {
      const distance = 100;
      let indicator = true;
      const timer = setInterval(() => {
        while (document.scrollingElement.scrollTop + window.innerHeight < document.scrollingElement.scrollHeight) {
          document.scrollingElement.scrollBy(0, distance);
          indicator = false;
        }

        if (indicator) {
          clearInterval(timer);
          resolve();
        }
        indicator = true;
      }, 10000);
    });
  });
};

const writeToFile = async data => {
  await fs.writeFileSync('data.json', data)
};


(async () => {
  const browser = await puppeteer.launch({
    headless: false
  });
  const page = await browser.newPage();
  await page.goto(playlistUrl);
  await page.setViewport({width: 800, height: 800});

  await autoScroll(page);

  const content = await page.content();
  const data = await parser(content);
  const str_data = await JSON.stringify(data);

  await writeToFile(str_data);
  await browser.close();
})();

