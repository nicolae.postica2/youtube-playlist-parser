import json
from time import sleep

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager


def get_driver() -> WebDriver:
    caps = DesiredCapabilities.CHROME
    caps['goog:loggingPrefs'] = {'performance': 'ALL'}
    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--window-size=1500,1000')
    return webdriver.Chrome(
        service=ChromeService(ChromeDriverManager().install()),
        options=options,
        desired_capabilities=caps
    )


def process_browser_log_entry(entry):
    response = json.loads(entry['message'])['message']
    return response


def export(data, filename="data.json", open_type="w"):
    print("[*] export data")
    with open(filename, open_type) as f:
        json.dump(data, f, indent=2, ensure_ascii=False)


class Driver:
    def __init__(self):
        print("[*] init driver")
        self.driver = get_driver()

    def tiktok_auth(self):
        pass

    def get_comments_log_count(self):
        count = 0
        browser_log = self.driver.get_log('performance')
        events = [process_browser_log_entry(entry) for entry in browser_log]
        for event in events:
            if event['params'].get('response'):
                if 'tiktok.com/api/comment/list' in event['params']['response']['url']:
                    count += 1
        return count

    def get_comments_from_tiktok_video(self, url: str) -> list:
        self.driver.get(url)
        sleep(3)

        data = []

        # scroll comments
        js_script = "window.scrollTo(0, document.body.scrollHeight);" \
                    "var lenOfPage=document.body.scrollHeight;" \
                    "return lenOfPage;"

        while True:
            self.driver.execute_script(js_script)
            sleep(3)

            # get comments data from cdp network
            browser_log = self.driver.get_log('performance')
            events = [process_browser_log_entry(entry) for entry in browser_log]
            local_data = []
            for event in events:
                if event['params'].get('response'):
                    if 'tiktok.com/api/comment/list' in event['params']['response']['url']:
                        request_id = event["params"]["requestId"]
                        try:
                            res = self.driver.execute_cdp_cmd('Network.getResponseBody', {'requestId': request_id})
                            body = json.loads(res['body'])
                            local_data.extend(body['comments'])
                        except:
                            pass
            if local_data:
                data.extend(local_data)
            else:
                break
        return data

    def close(self):
        print("[*] close driver")
        self.driver.close()
