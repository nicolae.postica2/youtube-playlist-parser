import json
import sys
import traceback

from services.driver import Driver


def main():
    urls = [
        "https://www.tiktok.com/@potrebprav/video/7009292673660030209",
        "https://www.tiktok.com/@potrebprav/video/7065692279159164161",
        "https://www.tiktok.com/@potrebprav/video/7058981783198534914",
        "https://www.tiktok.com/@potrebprav/video/7033376221350104321",
        "https://www.tiktok.com/@potrebprav/video/7012254639106166018",
        "https://www.tiktok.com/@potrebprav/video/7002935353195629826",
        "https://www.tiktok.com/@potrebprav/video/7053420489091353858",
        "https://www.tiktok.com/@potrebprav/video/6994799239549242626",
        "https://www.tiktok.com/@potrebprav/video/7007424160183880961",
        "https://www.tiktok.com/@potrebprav/video/6998893969958800642",
        "https://www.tiktok.com/@potrebprav/video/7016339634703584513",
        "https://www.tiktok.com/@potrebprav/video/7063456338776509698",
    ]
    urls_len = len(urls)
    driver = Driver()
    driver.driver.get("https://www.tiktok.com")
    input("Press ENTER for continue!")
    data = []
    for index, url in enumerate(urls, start=1):
        print(f"[ {index} : {urls_len}]")
        try:
            comments = driver.get_comments_from_tiktok_video(url=url)
            data.extend(comments)
            print(f"    |_ {len(comments)} new comments")
        except Exception:
            print("Exception in user code:")
            print("-" * 60)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            exe = repr(traceback.format_exception(exc_type, exc_value, exc_traceback))
            traceback.print_exc(file=sys.stdout)
            print("-" * 60)
    with open("comments.json", "w") as f:
        json.dump(data, f, indent=2, ensure_ascii=False)
    driver.close()


if __name__ == "__main__":
    # main()
    comments = []
    with open("comments.json", "r") as r:
        data = json.loads(r.read())
        print(len(data))
        for comment in data:
            print(comment["text"])
            comments.append({"body": comment["text"], 'class': "2"})

    with open("raw_comments.json", "w") as f:
        json.dump(comments, f, indent=2, ensure_ascii=False)
